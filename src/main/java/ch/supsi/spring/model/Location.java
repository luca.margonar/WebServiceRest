package ch.supsi.spring.model;

/**
 * Created by lucam on 25.04.2017.
 */
public class Location {
    private String lat;

    private String lng;

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }
}
