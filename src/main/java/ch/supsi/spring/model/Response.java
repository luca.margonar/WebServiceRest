package ch.supsi.spring.model;


/**
 * Created by lucam on 25.04.2017.
 */

public class Response {

    String address;
    String lat;
    String lng;
    String name;
    int quantity;
    int alarm;
    int prealarm;


    public Response() {
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getAlarm() {
        return alarm;
    }

    public void setAlarm(int alarm) {
        this.alarm = alarm;
    }

    public int getPrealarm() {
        return prealarm;
    }

    public void setPrealarm(int prealarm) {
        this.prealarm = prealarm;
    }

    public String getStatus() {

        if (getQuantity() <= getAlarm())
            return "alarm";
        else if (getQuantity() > getAlarm() && getQuantity() <= getPrealarm())
            return "prealarm";
        else if (getQuantity() > getPrealarm())
            return "available";
        else
            return "error";
    }


}
