package ch.supsi.spring.model;

/**
 * Created by lucam on 25.04.2017.
 */
public class GoogleResponse {


    private Result[] results ;
    private String status ;
    public Result[] getResults() {
        return results;
    }
    public void setResults(Result[] results) {
        this.results = results;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }



}
