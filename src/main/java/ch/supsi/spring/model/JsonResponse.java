package ch.supsi.spring.model;

/**
 * Created by Margonar on 26.04.2017.
 */
public class JsonResponse {
    String title;
    double lat;
    double lng;
    String description;

    public JsonResponse(String title, double lat, double lng, String description) {
        this.title = title;
        this.lat = lat;
        this.lng = lng;
        this.description = description;
    }
}
