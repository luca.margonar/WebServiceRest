package ch.supsi.spring.repository;

import ch.supsi.spring.model.Warehouse;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Margonar on 26.04.2017.
 */
@Repository
public interface WarehouseRepository extends CrudRepository<Warehouse,Long>{
    Warehouse findWarehouseByAddress(String address);
}
