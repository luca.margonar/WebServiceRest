package ch.supsi.spring.controller;

import ch.supsi.spring.model.GoogleResponse;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

/**
 * Created by lucam on 25.04.2017.
 */
public class CoordinatesConverter extends Converter {

    public GoogleResponse convertFromLatLong(String latlongString) throws IOException {
        URL url = new URL(URL + "?latlng=" + URLEncoder.encode(latlongString, "UTF-8") + "&sensor=false");

        URLConnection conn = url.openConnection();
        InputStream in = conn.getInputStream();
        ObjectMapper mapper = new ObjectMapper();
        GoogleResponse response = mapper.readValue(in, GoogleResponse.class);
        in.close();
        return response;
    }
}
