package ch.supsi.spring;

import ch.supsi.spring.controller.AddressConverter;
import ch.supsi.spring.controller.CoordinatesConverter;
import ch.supsi.spring.controller.CreateResponse;
import ch.supsi.spring.model.GoogleResponse;
import ch.supsi.spring.model.Response;
import ch.supsi.spring.model.Result;
import ch.supsi.spring.model.Warehouse;
import ch.supsi.spring.repository.WarehouseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Margonar on 26.04.2017.
 */

@RestController
public class EndPoint {

    List<String> addresses = new ArrayList<String>();
    List<String> coordinates = new ArrayList<String>();

    @Autowired
    protected WarehouseRepository warehouseRepository;



    @RequestMapping(value = "/addressConverter", method = RequestMethod.GET)
    public String addressConverter() throws IOException {
        if (warehouseRepository.findWarehouseByAddress("Via Gioacchino Respini, 6600 Locarno") == null) {
            List<Warehouse> warehouses = new ArrayList<>();
            Warehouse warehouse = new Warehouse();
            warehouse.setName("Magazzino Locarno");
            warehouse.setAddress("Via Gioacchino Respini, 6600 Locarno");
            warehouse.setAlarm(50);
            warehouse.setPrealarm(70);
            warehouse.setQuantity(75);
            Warehouse warehouse1 = new Warehouse();
            warehouse1.setName("Magazzino Lugano");
            warehouse1.setAddress("Via Industria 24, 6963 Pregassona");
            warehouse1.setAlarm(50);
            warehouse1.setPrealarm(70);
            warehouse.setQuantity(60);
            Warehouse warehouse2 = new Warehouse();
            warehouse2.setName("Magazzino Bellinzona");
            warehouse2.setAddress("Via Giuseppe Cattori 3, 6500 Bellinzona");
            warehouse2.setAlarm(50);
            warehouse2.setPrealarm(70);
            warehouse.setQuantity(60);
            warehouses.add(warehouse);
            warehouses.add(warehouse1);
            warehouses.add(warehouse2);
            warehouseRepository.save(warehouses);
        }
        List<Warehouse> warehouses = (ArrayList) warehouseRepository.findAll();
        for (Warehouse w : warehouses) {
            addresses.add(w.getAddress());
        }

        List<Response> response = new ArrayList<Response>();
        String lat = "";
        String lng = "";

        for (String address : addresses) {
            Response r = new Response();
            GoogleResponse res = new AddressConverter().convertToLatLong(address);
            if (res.getStatus().equals("OK")) {
                for (Result result : res.getResults()) {
                    lat = result.getGeometry().getLocation().getLat();
                    lng = result.getGeometry().getLocation().getLng();
                    r.setLat(lat);
                    r.setLng(lng);
                    System.out.println("Lattitude of address is :" + result.getGeometry().getLocation().getLat());
                    System.out.println("Longitude of address is :" + result.getGeometry().getLocation().getLng());
                    System.out.println("Location is " + result.getGeometry().getLocation_type());
                }
            } else {
                System.out.println(res.getStatus());
            }
            Warehouse w = warehouseRepository.findWarehouseByAddress(address);
            r.setName(w.getName());
            r.setAddress(address);
            r.setAlarm(w.getAlarm());
            r.setQuantity(w.getQuantity());
            r.setPrealarm(w.getPrealarm());
            response.add(r);
        }

        // Stringa da tornare al client
        String json = new CreateResponse().createResponse(response);
        return json;
    }

    @RequestMapping(value = "/addressConverter/{address}", method = RequestMethod.GET)
    public String singleAddressConverter(@PathVariable("address") String address) throws IOException {
        String lat = "";
        String lng = "";

        Response response = new Response();
        response.setAddress(address);
        GoogleResponse res = new AddressConverter().convertToLatLong(address);
        if (res.getStatus().equals("OK")) {
            for (Result result : res.getResults()) {
                lat = result.getGeometry().getLocation().getLat();
                lng = result.getGeometry().getLocation().getLng();
                response.setLat(lat);
                response.setLng(lng);
                System.out.println("Lattitude of address is :" + result.getGeometry().getLocation().getLat());
                System.out.println("Longitude of address is :" + result.getGeometry().getLocation().getLng());
                System.out.println("Location is " + result.getGeometry().getLocation_type());
            }
        } else {
            System.out.println(res.getStatus());
        }

        Warehouse w = warehouseRepository.findWarehouseByAddress(address);

        System.out.println(w.getId());


        response.setName(w.getName());
        response.setAlarm(w.getAlarm());
        response.setQuantity(w.getQuantity());
        response.setPrealarm(w.getPrealarm());
        List<Response> responses = new ArrayList<>();
        responses.add(response);


        // Stringa da tornare al client
        String json = new CreateResponse().createResponse(responses);
        return json;
    }

    @RequestMapping(value = "/coordinatesConverter", method = RequestMethod.GET)
    public String coordinatesConverter() throws IOException {
        coordinates.add("46.020964,8.965510499999999");
        coordinates.add("47.3768866,8.541694");
        coordinates.add("41.4036299,2.1743558");
        coordinates.add("40.7060006,-74.00880099999999");

        String response = "";
        for (String address : coordinates) {

            System.out.println("\n");
            GoogleResponse res1 = new CoordinatesConverter().convertFromLatLong(address);
            if (res1.getStatus().equals("OK")) {
                for (Result result : res1.getResults()) {
                    System.out.println("address is :" + result.getFormatted_address());
                }
                response += "[" + res1.getResults()[0].getFormatted_address() + "]";
            } else {
                System.out.println(res1.getStatus());
            }

        }

        return response;


    }
}
