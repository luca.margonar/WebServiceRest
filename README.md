<h1 style="color: green">Web Service Rest per la visualizzazione delle soglie in magazzino</h1>
<h3>Introduzione</h3>

Nell'ambito del progetto di semestre ho avuto la necessità di mostrare in un mappa le locazioni dei vari magazzini
con un messaggio che descrive lo stato del magazzino (alarm/prealarm/available).



<h3>Link for WS:</h3>

<b>Client: </b>http://isin03.dti.supsi.ch:9090/MARGONAR_LUCA/

<b>AddressConverter: </b>http://isin03.dti.supsi.ch:9090/MARGONAR_LUCA/addressConverter

<b>CordinatesConverter: </b>http://isin03.dti.supsi.ch:9090/MARGONAR_LUCA/coordinatesConverter

<b>AddressConverter/{Address}: </b>http://isin03.dti.supsi.ch:9090/MARGONAR_LUCA/addressConverter/Via%20Gioacchino%20Respini,%206600%20Locarno